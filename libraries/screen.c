#include "screen.h"



void base_graph_rectangle_fill_set(uint16_t addr, base_graph_param_t base_graph_param_temp[], uint8_t len)
{
    uint8_t cmd[252] = {0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0xE0, 0x03, 0x20, 0xAA, 0xAA, 0xFF, 0x00};
    uint8_t i = 0;

    if(len > 24)    //一次最多24个数据
        len = 24;

    //图形数量
    cmd[3] = len;

    for(i = 0;i < len;i++)
    {
        cmd[4 + i * 10] = base_graph_param_temp[i].start_x / 256;
        cmd[5 + i * 10] = base_graph_param_temp[i].start_x % 256;
        cmd[6 + i * 10] = base_graph_param_temp[i].start_y / 256;
        cmd[7 + i * 10] = base_graph_param_temp[i].start_y % 256;
        cmd[8 + i * 10] = base_graph_param_temp[i].end_x / 256;
        cmd[9 + i * 10] = base_graph_param_temp[i].end_x % 256;
        cmd[10 + i * 10] = base_graph_param_temp[i].end_y / 256;
        cmd[11 + i * 10] = base_graph_param_temp[i].end_y % 256;
        cmd[12 + i * 10] = base_graph_param_temp[i].color / 256;
        cmd[13 + i * 10] = base_graph_param_temp[i].color % 256;
    }

    cmd[len * 10 + 4] = 0xFF;
    cmd[len * 10 + 5] = 0x00;

    sys_write_vp(addr, cmd, (len * 10 + 6) / 2);
    
}

//文本变量设置
void text_display_value_set(uint16_t addr, uint16_t offset, char *text_data, uint16_t text_len)
{
    sys_write_vp(addr + offset, text_data, text_len);
}

//文本描述设置
void text_display_describe_set(uint16_t addr, uint32_t mask, uint16_t value_addr, int16_t x, int16_t y)
{
    if(mask & TEXT_MASK_VALUE_ADDR == TEXT_MASK_VALUE_ADDR)
    {
        sys_write_vp(addr, (uint8_t *)&value_addr, 1);
    }
}
