#ifndef __SHELL_H__
#define __SHELL_H__

#include "deconfig.h"
#include "dertos.h"

#define FINSH_PROMPT        finsh_get_prompt()
const char* finsh_get_prompt(void);

#ifndef FINSH_CMD_SIZE
#define FINSH_CMD_SIZE      30
#endif

#ifdef FINSH_USING_HISTORY
    #ifndef FINSH_HISTORY_LINES
        #define FINSH_HISTORY_LINES 5
    #endif
#endif

enum input_stat
{
    WAIT_NORMAL,
    WAIT_SPEC_KEY,
    WAIT_FUNC_KEY,
};

struct finsh_shell
{
    // struct de_semaphore rx_sem;

    enum input_stat stat;

    de_uint8_t echo_mode;

#ifdef FINSH_USING_HISTORY
    de_uint16_t current_history;
    de_uint16_t history_count;

    char cmd_history[FINSH_HISTORY_LINES][FINSH_CMD_SIZE];
#endif

#ifndef FINSH_USING_MSH_ONLY
    // struct finsh_parser parser;
#endif

    char line[FINSH_CMD_SIZE];
    de_uint8_t line_position;
    de_uint8_t line_curpos;

#ifndef RT_USING_POSIX
    // de_device_t device;
#endif

#ifdef FINSH_USING_AUTH
    char password[FINSH_PASSWORD_MAX];
#endif
};

#define BUFFER_SIZE 128        /* 环形缓冲区的大小 */

typedef struct
{
	unsigned int pR;           /* 读地址 */
	unsigned int pW;           /* 写地址 */   
    unsigned char buffer[BUFFER_SIZE];  /* 缓冲区空间 */    
} ring_buffer;


extern ring_buffer uart_rx_buf;

void shell_init(void);
void shell_task_entry(void);
void ring_buffer_write(unsigned char c, ring_buffer *dst_buf);

#endif
