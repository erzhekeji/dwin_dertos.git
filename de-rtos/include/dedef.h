#ifndef __DE_DEF_H__
#define __DE_DEF_H__

#include "deconfig.h"

#define uint8_t  unsigned char
#define uint16_t unsigned int
#define uint32_t unsigned long int

/* DE-RTOS basic data type definitions */
typedef signed   char                   de_int8_t;      /**<  8bit integer type */
typedef signed   short                  de_int16_t;     /**< 16bit integer type */
typedef signed   long                   de_int32_t;     /**< 32bit integer type */
typedef unsigned char                   de_uint8_t;     /**<  8bit unsigned integer type */
typedef unsigned short                  de_uint16_t;    /**< 16bit unsigned integer type */
typedef unsigned long                   de_uint32_t;    /**< 32bit unsigned integer type */
typedef int                             de_bool_t;      /**< boolean type */

typedef int                             de_base_t;      /**< Nbit CPU related date type */
typedef unsigned int                    de_ubase_t;     /**< Nbit unsigned CPU related data type */

typedef de_base_t                       de_err_t;       /**< Type for error number */
typedef de_uint32_t                     de_time_t;      /**< Type for time stamp */
typedef de_uint32_t                     de_tick_t;      /**< Type for tick count */
typedef de_base_t                       de_flag_t;      /**< Type for flags */
typedef de_ubase_t                      de_size_t;      /**< Type for size number */
typedef de_ubase_t                      de_dev_t;       /**< Type for device */
typedef de_base_t                       de_off_t;       /**< Type for offset */

/* boolean type definitions */
#define DE_TRUE                         1               /**< boolean true  */
#define DE_FALSE                        0               /**< boolean fails */


#define DE_NULL                         (0)

/**
 * Double List structure
 */
struct de_list_node
{
    struct de_list_node *next;                          /**< point to next node. */
    struct de_list_node *prev;                          /**< point to prev node. */
};
typedef struct de_list_node de_list_t;                  /**< Type for lists. */

#endif
