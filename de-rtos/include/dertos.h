#ifndef __DE_RTOS_H__
#define __DE_RTOS_H__

#include "dedef.h"
#include "deconfig.h"
#include "stdio.h"
#include "stdarg.h"

void *de_memmove(void *dest, const void *src, de_ubase_t n);
de_int32_t de_strncmp(const char *cs, const char *ct, de_ubase_t count);
de_int32_t de_strcmp(const char *cs, const char *ct);
de_size_t de_strlen(const char *src);
char *de_strdup(const char *s);

#endif
