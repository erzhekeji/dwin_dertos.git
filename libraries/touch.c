#include "touch.h"
#include "uart2.h"

#define TOUCH_HANDLE_MAX 10


bit is_btn_scan = 0;//是否要进行按钮点击扫描
bit is_sd_check = 0;

uint16_t touch_address;
uint8_t touch_function_cnt = 0;
touch_function_t touch_function[TOUCH_HANDLE_MAX];
uint16_t touch_values[TOUCH_HANDLE_MAX];

void touch_init(uint16_t touch_addr)
{
    touch_address = touch_addr;
}

void touch_insert(uint16_t touch_value, touch_function_t touch_function_temp)
{
    touch_function[touch_function_cnt] = touch_function_temp;
    touch_values[touch_function_cnt] = touch_value;
    touch_function_cnt++;
}

//用于产生按钮点击扫描信号,放在1ms的定时器中调用即可
void touch_tick()
{
	#define BTN_SCAN_PERIOD		100   //按钮点击事件的扫描周期,单位ms
	static idata uint8_t tick = 0;
	
	tick++;
	if(tick == BTN_SCAN_PERIOD)
	{
		tick = 0;
		is_btn_scan = 1;//产生一个按钮点击扫描信号
	}
}

//按钮点击事件扫描并处理
void touch_handler()
{
	uint16_t btn_val;
    uint8_t i = 0;
	
	if(is_btn_scan == 0)//是否有扫描信号,没有的话就啥也不做,直接返回
		return;
	is_btn_scan = 0;//清除
	
	//1.开始检查是否有按钮按下
	sys_read_vp(touch_address, (uint8_t*)&btn_val, 1);
	if(btn_val == 0)
		return;
	
	//2.按钮点击事件处理
	for(i = 0;i < touch_function_cnt;i++)
    {
        if(btn_val == touch_values[i])
        touch_function[i]();
    }

	//3.清除按键值
	btn_val = 0;
	sys_write_vp(touch_address, (uint8_t*)&btn_val, 1);
}

