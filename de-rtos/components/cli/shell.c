#include "shell.h"
#include "cmd.h"
#include "string.h"
#include "uart2.h"
#include "stdlib.h"


struct finsh_shell shell = {0};
ring_buffer uart_rx_buf;

/*
 *  函数名：void ring_buffer_init(ring_buffer *dst_buf)
 *  输入参数：dst_buf --> 指向目标缓冲区
 *  输出参数：无
 *  返回值：无
 *  函数作用：初始化缓冲区
*/
void ring_buffer_init(ring_buffer *dst_buf)
{
    dst_buf->pW = 0;
    dst_buf->pR = 0;
    memset(dst_buf, 0, FINSH_CMD_SIZE);
}

/*
 *  函数名：void ring_buffer_write(unsigned char c, ring_buffer *dst_buf)
 *  输入参数：c --> 要写入的数据
 *            dst_buf --> 指向目标缓冲区
 *  输出参数：无
 *  返回值：无
 *  函数作用：向目标缓冲区写入一个字节的数据，如果缓冲区满了就丢掉此数据
*/
void ring_buffer_write(unsigned char c, ring_buffer *dst_buf)
{
    int i = (dst_buf->pW + 1) % BUFFER_SIZE;
    if(i != dst_buf->pR)    // 环形缓冲区没有写满
    {
        dst_buf->buffer[dst_buf->pW] = c;
        dst_buf->pW = i;
    }
}

/*
 *  函数名：int ring_buffer_read(unsigned char *c, ring_buffer *dst_buf)
 *  输入参数：c --> 指向将读到的数据保存到内存中的地址
 *            dst_buf --> 指向目标缓冲区
 *  输出参数：无
 *  返回值：读到数据返回0，否则返回-1
 *  函数作用：从目标缓冲区读取一个字节的数据，如果缓冲区空了返回-1表明读取失败
*/
int ring_buffer_read(unsigned char *c, ring_buffer *dst_buf)
{
    if(dst_buf->pR == dst_buf->pW)
    {
        return -1;
    }
    else
    {
        *c = dst_buf->buffer[dst_buf->pR];
        dst_buf->pR = (dst_buf->pR + 1) % BUFFER_SIZE;
        return 0;
    }
}

const char *finsh_get_prompt()
{
#define _MSH_PROMPT "msh "
#define _PROMPT     "finsh "
    static char finsh_prompt[DE_CONSOLEBUF_SIZE + 1] = {0};

#ifdef FINSH_USING_MSH
    if (cmd_is_used()) 
        strcpy(finsh_prompt, _MSH_PROMPT);
    else
#endif
        strcpy(finsh_prompt, _PROMPT);

#if defined(RT_USING_DFS) && defined(DFS_USING_WORKDIR)
    /* get current working directory */
    getcwd(&finsh_prompt[de_strlen(finsh_prompt)], RT_CONSOLEBUF_SIZE - de_strlen(finsh_prompt));
#endif

    strcat(finsh_prompt, ">");

    return finsh_prompt;
}

#ifdef FINSH_USING_HISTORY
static de_bool_t shell_handle_history(struct finsh_shell *shell)
{
#if defined(_WIN32)
    int i;
    printf("\r");

    for (i = 0; i <= 60; i++)
        putchar(' ');
    printf("\r");

#else
    printf("\033[2K\r");
#endif
    printf("%s%s", FINSH_PROMPT, shell->line);
    return DE_FALSE;
}

static void shell_push_history(struct finsh_shell *shell)
{
    if (shell->line_position != 0)
    {
        /* push history */
        if (shell->history_count >= FINSH_HISTORY_LINES)
        {
            /* if current cmd is same as last cmd, don't push */
            if (memcmp(&shell->cmd_history[FINSH_HISTORY_LINES - 1], shell->line, FINSH_CMD_SIZE))
            {
                /* move history */
                int index;
                for (index = 0; index < FINSH_HISTORY_LINES - 1; index ++)
                {
                    memcpy(&shell->cmd_history[index][0],
                           &shell->cmd_history[index + 1][0], FINSH_CMD_SIZE);
                }
                memset(&shell->cmd_history[index][0], 0, FINSH_CMD_SIZE);
                memcpy(&shell->cmd_history[index][0], shell->line, shell->line_position);

                /* it's the maximum history */
                shell->history_count = FINSH_HISTORY_LINES;
            }
        }
        else
        {
            /* if current cmd is same as last cmd, don't push */
            if (shell->history_count == 0 || memcmp(&shell->cmd_history[shell->history_count - 1], shell->line, FINSH_CMD_SIZE))
            {
                shell->current_history = shell->history_count;
                memset(&shell->cmd_history[shell->history_count][0], 0, FINSH_CMD_SIZE);
                memcpy(&shell->cmd_history[shell->history_count][0], shell->line, shell->line_position);

                /* increase count and set current history position */
                shell->history_count ++;
            }
        }
    }
    shell->current_history = shell->history_count;
}
#endif

void shell_init(void)
{
    //shell = calloc(1, sizeof(struct finsh_shell));
    ring_buffer_init(&uart_rx_buf);

#ifdef FINSH_USING_MSH
    cmd_init();
#endif
}

char de_hw_console_getchar()
{
	char ch = 0;

    /* 从 ringbuffer 中拿出数据 */
    ring_buffer_read((unsigned char *)&ch, &uart_rx_buf);
    	
    return ch;
}

void shell_auto_complete(char *shell_line)
{
    printf("\r\n");
    cmd_complete(shell_line);
    printf("%s%s", FINSH_PROMPT, shell_line);
}

extern void shell_screen_append(uint8_t mode, char *line_data, uint8_t line_len);

void shell_task_entry()
{
    static uint8_t frist_flag = 0;
    char ch = 0;
    int8_t ret = 0;

    if(frist_flag == 0)
    {
        frist_flag = 1;
        shell.echo_mode = 1;
        printf(FINSH_PROMPT);
    }


    //while(1)
    {
        
        ch = de_hw_console_getchar();
        if(ch == 0)
        {
            return;
        }
            
        // printf("%c", ch);

        /*
         * handle control key
         * up key  : 0x1b 0x5b 0x41
         * down key: 0x1b 0x5b 0x42
         * right key:0x1b 0x5b 0x43
         * left key: 0x1b 0x5b 0x44
         */
        if (ch == 0x1b)
        {
            shell.stat = WAIT_SPEC_KEY;
            return;
        }
        else if (shell.stat == WAIT_SPEC_KEY)
        {
            if (ch == 0x5b)
            {
                shell.stat = WAIT_FUNC_KEY;
                return;
            }

            shell.stat = WAIT_NORMAL;
        }
        else if (shell.stat == WAIT_FUNC_KEY)
        {
            shell.stat = WAIT_NORMAL;

            if (ch == 0x41) /* up key */
            {
#ifdef FINSH_USING_HISTORY
                /* prev history */
                if (shell.current_history > 0)
                    shell.current_history --;
                else
                {
                    shell.current_history = 0;
                    return;
                }

                /* copy the history command */
                memcpy(shell.line, &shell.cmd_history[shell.current_history][0],
                       FINSH_CMD_SIZE);
                shell.line_curpos = shell.line_position = strlen(shell.line);
                shell_handle_history(&shell);
#endif
                return;
            }
            else if (ch == 0x42) /* down key */
            {
#ifdef FINSH_USING_HISTORY
                /* next history */
                if (shell.current_history < shell.history_count - 1)
                    shell.current_history ++;
                else
                {
                    /* set to the end of history */
                    if (shell.history_count != 0)
                        shell.current_history = shell.history_count - 1;
                    else
                        return;
                }

                memcpy(shell.line, &shell.cmd_history[shell.current_history][0],
                       FINSH_CMD_SIZE);
                shell.line_curpos = shell.line_position = strlen(shell.line);
                shell_handle_history(&shell);
#endif
                return;
            }
            else if (ch == 0x44) /* left key */
            {
                if (shell.line_curpos)
                {
                    printf("\b");
                    shell.line_curpos --;
                }

                return;
            }
            else if (ch == 0x43) /* right key */
            {
                if (shell.line_curpos < shell.line_position)
                {
                    printf("%c", shell.line[shell.line_curpos]);
                    shell.line_curpos ++;
                }

                return;
            }
        }

        /* received null or error */
        if (ch == '\0' || ch == 0xFF) return;
        /* handle tab key */
#ifdef FINSH_USING_SYMTAB
        else if (ch == '\t')
        {
            int i;
            /* move the cursor to the beginning of line */
            for (i = 0; i < shell.line_curpos; i++)
                printf("\b");

            /* auto complete */
            shell_auto_complete(&shell.line[0]);
            /* re-calculate position */
            shell.line_curpos = shell.line_position = strlen(shell.line);

            return;
        }
#endif
        /* handle backspace key */
        else if (ch == 0x7f || ch == 0x08)
        {
            /* note that shell.line_curpos >= 0 */
            if (shell.line_curpos == 0)
                return;

            shell.line_position--;
            shell.line_curpos--;

            if (shell.line_position > shell.line_curpos)
            {
                int i;

                de_memmove(&shell.line[shell.line_curpos],
                           &shell.line[shell.line_curpos + 1],
                           shell.line_position - shell.line_curpos);
                shell.line[shell.line_position] = 0;

                printf("\b%s  \b", &shell.line[shell.line_curpos]);

                /* move the cursor to the origin position */
                for (i = shell.line_curpos; i <= shell.line_position; i++)
                    printf("\b");
            }
            else
            {
                printf("\b \b");
                shell.line[shell.line_position] = 0;
            }

            return;
        }

        /* handle end of line, break */
        if (ch == '\r' || ch == '\n')
        {
#ifdef FINSH_USING_HISTORY
            shell_push_history(&shell);
#endif

#ifdef FINSH_USING_MSH
            
            shell_screen_append(0, shell.line, shell.line_position);
            if (cmd_is_used() == DE_TRUE)
            {
                if (shell.echo_mode)
                    printf("\r\n");
                cmd_exec(shell.line, shell.line_position);
            }
            else
#else
            printf("\r\n");
#endif
            {
#ifndef FINSH_USING_MSH_ONLY
                /* add ';' and run the command line */
                shell.line[shell.line_position] = ';';

                if (shell.line_position != 0) 
                    finsh_run_line(&shell.parser, shell.line);
                else
                    if (shell.echo_mode) printf("\n");
#endif
            }
            printf(FINSH_PROMPT);
            memset(shell.line, 0, sizeof(shell.line));
            shell.line_curpos = shell.line_position = 0;
            return;
        }

        /* it's a large line, discard it */
        if (shell.line_position >= FINSH_CMD_SIZE)
            shell.line_position = 0;

        /* normal character */
        if (shell.line_curpos < shell.line_position)
        {
            int i;

            de_memmove(&shell.line[shell.line_curpos + 1],
                       &shell.line[shell.line_curpos],
                       shell.line_position - shell.line_curpos);
            shell.line[shell.line_curpos] = ch;
            if (shell.echo_mode)
                printf("%s", &shell.line[shell.line_curpos]);

            /* move the cursor to new position */
            for (i = shell.line_curpos; i < shell.line_position; i++)
                printf("\b");
        }
        else
        {
            shell.line[shell.line_position] = ch;
            if (shell.echo_mode)
                printf("%c", ch);
        }

        ch = 0;
        shell.line_position ++;
        shell.line_curpos++;
        if (shell.line_position >= FINSH_CMD_SIZE)
        {
            /* clear command line */
            shell.line_position = 0;
            shell.line_curpos = 0;
        }
    } /* end of device read */
}