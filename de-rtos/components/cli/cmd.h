#ifndef __CMD_H__
#define __CMD_H__

#include "dertos.h"

void cmd_init(void);
de_bool_t cmd_is_used(void);
void cmd_complete(char *cmd_line);
void cmd_exec(char *cmd_line, uint8_t cmd_len);

#endif
