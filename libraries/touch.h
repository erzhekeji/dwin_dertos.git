#ifndef __TOUCH_H__
#define __TOUCH_H__

#include "sys.h"

typedef void (*touch_function_t)();

void touch_init(uint16_t touch_addr);
void touch_insert(uint16_t touch_value, touch_function_t touch_function_temp);
void touch_tick();
void touch_handler();

#endif
