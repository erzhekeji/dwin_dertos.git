#ifndef __UART2_H__
#define __UART2_H__

#include "sys.h"
#include "stdio.h"

#define UART2_INT_EN							1				//串口中断是否使能

//函数申明
void uart2_init(u32 baud);
void uart2_send_byte(u8 byte);
void uart2_send_bytes(u8 *bytes,u16 len);

#endif

